package drive;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.util.FileManager;
import crawler.ImmoClient;
import crawler.OverpassCrawler;
import javafx.util.Pair;
import model.FlatFactory;
import model.OPCFactory;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Category;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import util.*;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import static util.Namespace.*;

/**
 * Created by Andreas Kluge on 31.05.2015.
 *
 * Entry Point Class
 */
public class Drive {

    private static final Logger logger = Logger.getLogger(Drive.class);

    private static  boolean skipGet = false;
    private static  boolean skipCable = false;
    private static boolean keepFiles = false;

    /**
     * Main method, keeps working order
     *
     * @param args, for parameter usage
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        BasicConfigurator.configure();

        Category root = logger.getParent();
        root.setLevel(Level.INFO);
        logger.setLevel(Level.INFO);

        String consumerKey = "";
        String consumerSecret = "";

        if (args.length >= 2) {
            consumerKey = args[0];
            consumerSecret = args[1];
        } else {
            System.out.println("Usage: program <consumer key> <consumer secret>");
            System.exit(-1);
        }

        int c = 0;
        if (args.length > 2){
            for (int i = 2; i < args.length; i++) {
                if (args[i].equals("-skipGet")) {
                    skipGet = true;
                    c++;
                }
                if (args[i].equals("-skipCable")) {
                    skipCable = true;
                    c++;
                }
                if (args[i].equals("-keepFiles")) {
                    keepFiles = true;
                    c++;
                }
            }
        }


        if (args.length-2 != c){
            System.out.println("Usage: program <consumer key> <consumer secret>");
            logger.warn("allowed additional parameter:");
            logger.warn("-skipGet            disable resource crawler");
            logger.warn("-skipCable          disable cable crawler");
            logger.warn("-keepFiles          keep resource .xml-Files (RDF will not affected)");
            System.exit(-1);
        }


        long start = System.currentTimeMillis();

        if (!skipGet) {
            logger.info("Get Resource Information");
            queryResources(consumerKey, consumerSecret);
        } else {
            File flatFile = new File(FLAT_SERIALISATION_NAME);
            File bakeryFile = new File(BAKERY_FILE_NAME + EXTENSION);
            File superMarketFile = new File(SUPERMARKET_FILE_NAME + EXTENSION);
            File stopFile = new File(PUBLIC_TRANSPORT_FILE_NAME + EXTENSION);

            if (!flatFile.exists()
                    || !bakeryFile.exists()
                    || !superMarketFile.exists()
                    || !stopFile.exists()) {
                logger.error("can't skip get, while missing file(s)");
                System.exit(-1);
            }
        }

        logger.info("create RDF");

        ArrayList<OntModel> models = createRDF();
        logger.info("create RDF finished");

        OntModel m = models.get(3);
        String queryString = FileManager.get().readWholeFileAsUTF8("query");
        logger.warn( queryString);
        Query query = QueryFactory.create(queryString);

        Model model1 = m.union(models.get(0));
        Model model2 = model1.union(models.get(1));
        Model model = model2.union(models.get(2));

        try {
            QueryExecution qe = QueryExecutionFactory.create(query,model);
            ResultSet result = qe.execSelect();

            String resultWithDataTypes = ResultSetFormatter.asText(result);

            String resNoDT = printResultWithoutDataTypes(resultWithDataTypes);

            logger.debug("length compare"+resultWithDataTypes.length() + "    " + resNoDT.length());

            if (resultWithDataTypes.length() == resNoDT.length())
                logger.info("no solution found");
          //  ResultSetFormatter.out(System.out, result);

        } catch (Throwable throwable){
            logger.info("query Failed");
        }


        logger.debug(deleteDumbFiles(DUMMY_FILE_NAME+EXTENSION));
        if (!keepFiles) {
            logger.debug(deleteDumbFiles(BAKERY_FILE_NAME + EXTENSION));
            logger.debug(deleteDumbFiles(FLAT_FILE_NAME + EXTENSION));
            logger.debug(deleteDumbFiles(SUPERMARKET_FILE_NAME + EXTENSION));
            logger.debug(deleteDumbFiles(PUBLIC_TRANSPORT_FILE_NAME+EXTENSION));
        }

        logger.info("finished");

        long stop = System.currentTimeMillis();
        printTime(start, stop);
    }

    private static boolean deleteDumbFiles(String filename) {
        File file = new File(filename);
        boolean erg = false;
        if (file.exists())
            erg = file.delete();
        return erg;
    }

    /**
     * Remove RDF DataTypes and Fix Header
     * @param str, String from ResultSetFormatter with Values and DataTypes
     * @return String with Values but without DataTypes
     */
    private static String printResultWithoutDataTypes(String str) {
        //Removing data types and quotes
        String content = str.replaceAll("\\^\\^<[a-zA-Z0-9:/.#]+>", "");
        content = content.replaceAll("\"","");

        // extract header line
        String[] splitContent = content.split("\\n");
        String header = splitContent[1];
        String referenceLine = splitContent[splitContent.length-2];

        header = header.replaceAll("  ", "");

        int next = 0;
        int lastOf = referenceLine.lastIndexOf(" | ");
        if (lastOf  == -1)
            return content;
        while (next != lastOf) {
            int pos = referenceLine.indexOf(" | ", next+1);
            int fix = header.indexOf(" | ", next+1);

            int diff = pos - fix;
            for (int i = 0; i < diff; i++) {
                String foo1 = header.substring(0, fix);
                String foo2 = header.substring(fix);
                header = foo1 + " " + foo2;
            }
            next = pos;
        }

        header = header.substring(0,header.lastIndexOf("|"));
        int len = header.length();
        for (int i = len; i < referenceLine.length();i++){
            header = header+" ";
        }

        header = header.substring(0,header.length()-2);
        header = header+"|";

        splitContent[1] = header;

        splitContent[0] = splitContent[0].substring(0,header.length());
        splitContent[2] = splitContent[2].substring(0,header.length());
        splitContent[splitContent.length-1] = splitContent[splitContent.length-1].substring(0,header.length());

        for (String aSplitContent : splitContent) {
            System.out.println(aSplitContent);
        }
        return content;
    }

    /**
     * wrapper method for RDF model and file creation
     *
     * @return list of separated ontology models
     */
    private static ArrayList<OntModel> createRDF() {
        long start = System.currentTimeMillis();

        String bakery = "bakery";
        String supermarket = "supermarket";
        String publicTransport = "stop_position";
        OntModel rdfBakery;
        OntModel rdfSupermarket;
        OntModel rdfPublicTransport;
        OntModel rdfFlat;

        ArrayList<OntModel> models = new ArrayList<>();
        try {
            rdfBakery = OPCFactory.createOPCRDFSchema(bakery);
            models.add(rdfBakery);
            rdfSupermarket = OPCFactory.createOPCRDFSchema(supermarket);
            models.add(rdfSupermarket);
            rdfPublicTransport = OPCFactory.createOPCRDFSchema( publicTransport );
            models.add(rdfPublicTransport);

            rdfFlat = FlatFactory.createFlatRDFSchema();
            models.add(rdfFlat);
        } catch (IOException | SAXException | ParserConfigurationException e) {
            logger.error(e.getMessage());
            System.exit(-1);
        }

        long stop = System.currentTimeMillis();
        printTime(start, stop);

        return models;
    }

    /**
     * Method for calling all crawlers to get resource information.
     *
     * @throws IOException
     * @param consumerKey String with account information
     * @param consumerSecret String with account information
     */
    private static void queryResources(String consumerKey, String consumerSecret) throws IOException {
        long start = System.currentTimeMillis();
        logger.info("get Flats");
        List<Flat> flats = getFlats(consumerKey, consumerSecret );
        logger.info("get Flats done");
        /* end of immoscout24 crawler */

        if (!skipCable) {
        /* cable information */
            logger.info("is cable available");
            Cable ca = new Cable(flats);
            ca.runCableCheck();
            logger.info("runCableCheck done");
        }
        /* get bakeries, supermarkets, public transport stations */
        logger.info("get bakeries, supermarkets, stop stations ");
        OverpassCrawler opc = new OverpassCrawler();
        opc.run();
        logger.info("opc done");

        /* finished gathering  */
        DistantCalculator dc = new DistantCalculator(opc.getPublicTransports(), opc.getSupermarkets(), opc.getBakeries());
        dc.addFlats(flats);
        flats = dc.run();

        try {
            FileOutputStream fileOut = new FileOutputStream(FLAT_SERIALISATION_NAME);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(flats);
            out.close();
            fileOut.close();
            logger.info("full flat information serialized");
        }catch(IOException i) {
            i.printStackTrace();
        }

        long stop = System.currentTimeMillis();
        printTime(start, stop);
    }

    /**
     * Get immoscout24 Estate information, create Flat object's
     * save Flat object's to Filesystem in XML - Style
     *
     * @return List with Flats form immoscout24
     * @throws IOException
     * @param consumerKey String with account information
     * @param consumerSecret String with account information
     */
    private static List<Flat> getFlats(String consumerKey, String consumerSecret) throws IOException {
        ImmoClient client = new ImmoClient(consumerKey, consumerSecret);
        client.requestQuery();

        List<String> exposeIds = client.getmexposeIds();
        List<StringBuffer> bufferList = client.getBufferList();
        StringBuffer bufferAll = new StringBuffer();
        bufferAll.append(XML_HEAD);
        bufferAll.append(LINE_SEPARATOR);
        bufferAll.append("<SET>");
        bufferAll.append(LINE_SEPARATOR);

        String exposeID;
        StringBuffer b;
        List<Flat> flats = new ArrayList<>();
        int bufferListSize = bufferList.size();
        for (int i = 0; i < bufferListSize; i++) {
            exposeID = exposeIds.get(i);
            String realEstateHead = REAL_ESTATE_XMLNS_XSI
                    + XML_SCHEMA + exposeID + "\">";
            bufferAll.append(realEstateHead);
            bufferAll.append(LINE_SEPARATOR);
            b = bufferList.get(i);
            FileWriter.writeToFile(b, DUMMY_FILE_NAME, EXTENSION);

            Pair<StringBuffer, Flat> pair = null;

            try {
                pair = Reducer.reduceExposeToRelevant(new File(DUMMY_FILE_NAME + EXTENSION).getAbsolutePath());
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }
            if (pair != null) {
                bufferAll.append(pair.getKey()); // Estate with reduced information
                if (pair.getValue() != null) {
                    Flat flat = pair.getValue();
                    flat.setExposeID(Integer.parseInt(exposeID)); // set exposeID to flat object
                    flats.add(flat);
                }
                bufferAll.append(REAL_ESTATE_FOOT);
            }
        }
        bufferAll.append("</SET>");
        FileWriter.writeToFile(bufferAll, FLAT_FILE_NAME, EXTENSION);

        return flats;
    }

    /**
     * Method for clocking
     *
     * @param start, startTime
     * @param stop, endTime
     */
    private static void printTime(long start, long stop) {
        long timeNeeded = stop - start;
        logger.debug("Start : " + start);
        logger.debug("Stop : " + stop);
        logger.info("Duration : " + timeNeeded + "ms");
        long hour = timeNeeded / 3600000;
        long min = (timeNeeded / 60000) % 60;
        long sec = (timeNeeded / 1000) % 60;
        if (min > 0 )
            logger.info("Duration : " + hour + "h" + min + "min" + sec + "sec");

    }

}
