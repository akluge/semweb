package crawler;

import util.OPCObjectType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static util.FileWriter.writeToFile;
import static util.Namespace.EXTENSION;
import static util.Namespace.LINE_SEPARATOR;

/**
 * Created by Kluge Andreas on 04.08.2015.
 *
 * OpenStreetMap crawler
 */
public class OverpassCrawler {
    private static final String OVER_PASS_URL = "http://overpass-api.de/api/interpreter?";
    private static final String CITY = "Leipzig";
    private static final String OVER_PASS_QUERY_PUBLIC_TRANSPORT = "data=area[name=" + CITY + "];node(area)[public_transport=stop_position];out;";
    private static final String OVER_PASS_QUERY_SUPERMARKET = "data=area[name=" + CITY + "];node(area)[shop=supermarket];out;";
    private static final String OVER_PASS_QUERY_BAKERY = "data=area[name=" + CITY + "];node(area)[shop=bakery];out;";
    private static final String STOP_POSITION = "stop_position";
    private static final String SUPERMARKET = "supermarket";
    private static final String BAKERY = "bakery";

    private List<OPCObjectType> publicTransports;
    private List<OPCObjectType> supermarkets;
    private List<OPCObjectType> bakeries;

    public OverpassCrawler() {
    }

    /**
     *  crawler publicTransport stops, bakeries and supermarkets for chosen city
     */
    public void run(){
        try {
            StringBuffer publicTransport = getXMLAndWriteFile(STOP_POSITION, OVER_PASS_QUERY_PUBLIC_TRANSPORT);
            publicTransports = extractOPCObjectFromStringBuffer(publicTransport);

            StringBuffer supermarket = getXMLAndWriteFile(SUPERMARKET, OVER_PASS_QUERY_SUPERMARKET);
            supermarkets = extractOPCObjectFromStringBuffer(supermarket);

            StringBuffer bakery = getXMLAndWriteFile(BAKERY, OVER_PASS_QUERY_BAKERY);
            bakeries = extractOPCObjectFromStringBuffer(bakery);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * extract openStreetMap objects
     *
     * @param stringBuffer contains openStreetMap query result
     * @return list of parsed openStreetMap objects
     */
    private List<OPCObjectType> extractOPCObjectFromStringBuffer(StringBuffer stringBuffer) {
        List<OPCObjectType> opcObjectTypes = new ArrayList<>();
        String name ="";
        long id = 0;
        String typ = "";
        double lat = 0.0;
        double lon = 0.0;

        String splitLines[];
        String[] lines = stringBuffer.toString().split(LINE_SEPARATOR);
        int noHead = 5;
        boolean inline =false;
        for (int i = noHead; i < lines.length; i++) {
            String line = lines[i];

            if (line.contains("node") && line.contains("id=")){
                inline = true;
            }

            if (inline){
                if (line.contains("id=")){
                    splitLines = line.split("id=\"");
                    id = Long.parseLong(splitLines[1].substring(0, splitLines[1].indexOf("\" lat=")));
                    splitLines = splitLines[1].split("lat=\"");
                    lat = Double.parseDouble(splitLines[1].substring(0,splitLines[1].indexOf("\" lon")));
                    splitLines = splitLines[1].split("lon=\"");
                    lon = Double.parseDouble(splitLines[1].substring(0, splitLines[1].indexOf("\">")));
                }
                if (line.contains("k=\"name")){
                    splitLines = line.split("v=");
                    name = splitLines[1].substring(1, splitLines[1].length() - 3);
                }
                if (line.contains("k=\"shop") || line.contains("k=\"public_transport")){
                    splitLines = line.split("v=");
                    typ = splitLines[1].substring(1,splitLines[1].length() - 3);

                }
            }
            if (line.contains("/node>") && inline) {
                inline = false;
                opcObjectTypes.add(new OPCObjectType(name, id, typ, lat, lon));
            }
        }

        return opcObjectTypes;
    }

    /**
     * query OpenStreetMap
     *
     * @param fileName, for file writing
     * @param queryString, contains query parameter
     * @return StringBuffer with query result in XML style
     * @throws IOException
     */
    private StringBuffer getXMLAndWriteFile(String fileName, String queryString) throws IOException {
        StringBuffer content = new StringBuffer();
        URL url = new URL(OVER_PASS_URL+queryString );

        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), StandardCharsets.UTF_8));
        String buffer;

        while ((buffer = in.readLine() ) !=null){
            content.append(buffer);
            content.append(LINE_SEPARATOR);
        }

        in.close();
        writeToFile(content, fileName, EXTENSION);
        return content;
    }

    public List<OPCObjectType> getPublicTransports() {
        return publicTransports;
    }

    public List<OPCObjectType> getSupermarkets() {
        return supermarkets;
    }

    public List<OPCObjectType> getBakeries() {
        return bakeries;
    }
}

