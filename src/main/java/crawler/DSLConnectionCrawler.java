package crawler;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import util.Address;

import java.io.IOException;


/**
 * Created by Kluge Andreas on 11.08.2015.
 * check cable availability
 */
public class DSLConnectionCrawler {

    /* query string */
    private static final String HTTPS_WWW_PRIMACOM_DE_AVAILABILITY_CHECK_ADDRESS = "https://www.primacom.de/availability/check-address";
    /* query parameter */
    private static final String TOKEN = "_token";
    private static final String LOCATION = "location";
    private static final String STREET = "street";
    private static final String NUMBER = "number";
    private static final String MOZILLA = "Mozilla";
    private static final String CITY = "+Leipzig";
    private static final String TOKEN_VALUE = "13e9c15113701d056244f12ad401f194";

    public DSLConnectionCrawler() {

    }

    /**
     * check if cable is available for current address
     *
     * @param address, contains location info for querying
     * @return True, if redirect url leads to success Page
     */
    public boolean isPrimaComAvailable(Address address) {
        Connection.Response response;
        boolean erg = false;

        try {
            response = Jsoup.connect(HTTPS_WWW_PRIMACOM_DE_AVAILABILITY_CHECK_ADDRESS)
                    .data(TOKEN, TOKEN_VALUE)
                    .data(LOCATION, address.getPostcode() + CITY)
                    .data(STREET, address.getStreet())
                    .data(NUMBER, address.getHouseNumber())
                    .userAgent(MOZILLA)
                    .timeout(30000)
                    .method(Connection.Method.POST)
                    .followRedirects(true)
                    .execute();
            Document document = response.parse();

            String body = document.body().html();

            if (body.contains("success")) {
                //right redirect url
                //if url contains "none" the test result is negative
                erg = !body.contains("none");
            }
        } catch (IOException ex) {
            System.out.println("Error with "
                    + address.getPostcode() + " "
                    + address.getStreet() + " "
                    + address.getHouseNumber());

            System.out.println("1hour break -- anti crawler measures");

            try {
                Thread.sleep(3600000); //wait 1h cause anti crawler measures
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("break is over");

            erg = isPrimaComAvailable(address);
        }

        return erg;
    }
}
