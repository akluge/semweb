package crawler;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.basic.DefaultOAuthProvider;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ImmoScout24Setup {

	private static final String SERVER_URL = "http://rest.immobilienscout24.de";
	private static final String OAUTH_SECURITY_PREFIX = "/restapi/security/oauth/";
	private static final String AUTHORIZATION_ENDPOINT = SERVER_URL + OAUTH_SECURITY_PREFIX + "confirm_access";
	private static final String ACCESS_TOKEN_ENDPOINT = SERVER_URL + OAUTH_SECURITY_PREFIX + "access_token";
	private static final String REQUEST_TOKEN_ENDPOINT = SERVER_URL + OAUTH_SECURITY_PREFIX + "request_token";

	public static void mainTestBla(String[] args) throws Throwable {
		if (args.length == 2) {
			final String consumerKey = args[0];
			final String consumerSecret = args[1];
			final OAuthConsumer consumer = new DefaultOAuthConsumer(consumerKey, consumerSecret);
			final OAuthProvider provider = new DefaultOAuthProvider(REQUEST_TOKEN_ENDPOINT, ACCESS_TOKEN_ENDPOINT, AUTHORIZATION_ENDPOINT);

			connectAccount(consumer, provider);
		} else {
			System.out.println("Usage: program <consumer key> <consumer secret>");
		}
	}

	/**
	 *
	 *
	 * @param consumer String with account information
	 * @param provider String with account information
	 * @throws OAuthMessageSignerException
	 * @throws OAuthNotAuthorizedException
	 * @throws OAuthExpectationFailedException
	 * @throws OAuthCommunicationException
	 * @throws IOException
	 */
	private static void connectAccount(final OAuthConsumer consumer, final OAuthProvider provider) throws IOException, OAuthMessageSignerException,
			OAuthNotAuthorizedException, OAuthExpectationFailedException, OAuthCommunicationException {

		System.out.println("Fetching request token from " + REQUEST_TOKEN_ENDPOINT);
		final String authUrl = provider.retrieveRequestToken(consumer, OAuth.OUT_OF_BAND);

		System.out.println("Request token: " + consumer.getToken());

		System.out.println("Token secret: " + consumer.getTokenSecret());

		// Now we must grant the request token (out of band): use a web browser
		// or a web view for that approach.

		// After a successful login or registration (to identify an user) you
		// can give our application the required permissions.

		// Don't save the user credentials (username, password) on the client!

		System.out.println("Please visit " + authUrl + ",\nlogin or register and grant " + "this application authorization. Please copy the PIN code to "
				+ "obtain an access token.");

		System.out.println("Enter the PIN code and hit ENTER when you're done:");

		final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		final String pin = br.readLine();

		br.close();

		// todo: use finally to ensure to free resources

		System.out.println("Fetching access token from " + ACCESS_TOKEN_ENDPOINT);

		// Retrieve the access token for the given consumer granted with the

		// given pin; this is required only once.

		provider.retrieveAccessToken(consumer, pin);

		System.out.println("Access token: " + consumer.getToken());

		System.out.println("Token secret: " + consumer.getTokenSecret());

		// Make an API call... it is required to sign each request (see below)

		// final URL url = new URL(SERVER + SEARCH_PREFIX + "region" +
		// "?q=Leipzig");
	}

}
