package crawler;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.basic.DefaultOAuthProvider;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import util.FailedRequestException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImmoClient {
	private static final String SERVER_URL = "http://rest.immobilienscout24.de";
	private static final String OAUTH_SECURITY_PREFIX = "/restapi/security/oauth/";

	// Key->Secret bei Registrierung
	private static String CONSUMER_KEY = "";
	private static String CONSUMER_SECRET = "";
	// ---------------------------------------------------------
	// Erst nach Connect
	private static final String ACCOUNT_ID = "";
	private static final String ACCOUNT_KEY = "";

	private static final String AUTHORIZATION_ENDPOINT = SERVER_URL
			+ OAUTH_SECURITY_PREFIX + "confirm_access";
	private static final String ACCESS_TOKEN_ENDPOINT = SERVER_URL
			+ OAUTH_SECURITY_PREFIX + "access_token";
	private static final String REQUEST_TOKEN_ENDPOINT = SERVER_URL
			+ OAUTH_SECURITY_PREFIX + "request_token";
	// technical and business context of the search webservice
	private static final String SEARCH_PREFIX = "/restapi/api/search/v1.0/";

	private final Pattern currentPageNumberPattern = Pattern
			.compile("^.*<pageNumber>([0-9]+)</pageNumber>.*$");
	private final Pattern totalPageNumberPattern = Pattern
			.compile("^.*<numberOfPages>([0-9]+)</numberOfPages>.*$");
	private final Pattern exposeIdPattern = Pattern
			.compile("^.*<resultlistEntry xlink:href=\".+\" id=\"([0-9]+)\".*$");

	private final List<String> mexposeIds;
	private final List<StringBuffer> bufferList;
	public ImmoClient(String key, String secret) {

		CONSUMER_KEY = key;
		CONSUMER_SECRET = secret;

		mexposeIds = new ArrayList<>();
		bufferList = new ArrayList<>();
	}

	public List<StringBuffer> getBufferList() {
		return bufferList;
	}

	public void requestQuery() throws IOException {
		final OAuthConsumer consumer = new DefaultOAuthConsumer(CONSUMER_KEY,
				CONSUMER_SECRET);
		final OAuthProvider provider = new DefaultOAuthProvider(
				REQUEST_TOKEN_ENDPOINT, ACCESS_TOKEN_ENDPOINT,
				AUTHORIZATION_ENDPOINT);

		provider.setOAuth10a(true);
		consumer.setTokenWithSecret(ACCOUNT_ID, ACCOUNT_KEY);

		String mCityGeoCode = "1276013012";
		final String urlString = SERVER_URL + SEARCH_PREFIX
				+ "search/region?realestatetype=apartmentrent&geocodes="
				+ mCityGeoCode + "&channel=23&pagesize=200";

		String suffix = "";
		int page = 1;

		try {
			while (requestFlatsPerPage(consumer, new URL(urlString + suffix))) {
				page++;
				suffix = "&pagenumber=" + page;
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			System.out.println("Entries found: " + mexposeIds.size());
			for (String exposeId : mexposeIds) {
				bufferList.add(requestexposeById(consumer, exposeId));

				// XML der aktuellen Wohnung in root
				//root.toString();
/*
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
*/
			}

		} catch (FailedRequestException e) {
			e.printStackTrace();
		}

	}

	/**
	 *
	 * @param consumer contains CurrentUser
	 * @param url contains queryURL
	 * @return True, if more entries available on the next page
	 * @throws FailedRequestException
	 */
	private boolean requestFlatsPerPage(final OAuthConsumer consumer,
			final URL url) throws FailedRequestException {
		HttpURLConnection request = null;
		try {
			request = (HttpURLConnection) url.openConnection();
			// request.setRequestProperty("ACCEPT", "application/json");
			request.setUseCaches(false);
			request.setRequestMethod("GET");
			try {
				consumer.sign(request);
			} catch (OAuthMessageSignerException
					| OAuthExpectationFailedException
					| OAuthCommunicationException e) {
				throw new FailedRequestException("OAuth failed", e);
			}

			request.connect();
			if (request.getResponseCode() == HttpURLConnection.HTTP_OK) {
				int currentPage = 1;
				int totalPage = 1;
				try (final BufferedReader reader = new BufferedReader(
						new InputStreamReader(request.getInputStream(),
								Charset.forName("UTF-8")), 8192)) {
					String line;

					while ((line = reader.readLine()) != null) {
						final Matcher exposeMatcher = exposeIdPattern
								.matcher(line);
						if (exposeMatcher.matches()) {
							mexposeIds.add(exposeMatcher.group(1));
							continue;
						}
						final Matcher currentPageMatcher = currentPageNumberPattern
								.matcher(line);
						if (currentPageMatcher.matches()) {
							currentPage = Integer.parseInt(currentPageMatcher
									.group(1));
							continue;
						}
						final Matcher totalPageMatcher = totalPageNumberPattern
								.matcher(line);
						if (totalPageMatcher.matches()) {
							totalPage = Integer.parseInt(totalPageMatcher
									.group(1));
						}
					}
				}
				return currentPage < totalPage;
			} else {
				throw new FailedRequestException(request.getResponseCode()
						+ ": " + request.getResponseMessage() + "\n"
						+ request.toString());
			}
		} catch (IOException e) {
			throw new FailedRequestException("Flat request failed", e);
		} finally {
			if (request != null) {
				request.disconnect();
			}
		}
	}

	private StringBuffer requestexposeById(final OAuthConsumer consumer,
											final String exposeId) throws FailedRequestException {
		HttpURLConnection request = null;
		URL url;
		try {
			url = new URL(
					"http://rest.immobilienscout24.de/restapi/api/search/v1.0/expose/"
							+ exposeId);
		} catch (MalformedURLException e) {
			throw new FailedRequestException("Build request url", e);
		}
		try {
			request = (HttpURLConnection) url.openConnection();
			request.setUseCaches(false);
			request.setRequestMethod("GET");
			try {
				consumer.sign(request);
			} catch (OAuthMessageSignerException
					| OAuthExpectationFailedException
					| OAuthCommunicationException e) {
				throw new FailedRequestException("OAuth failed", e);
			}

			request.connect();
			if (request.getResponseCode() == HttpURLConnection.HTTP_OK) {
				try (final BufferedReader reader = new BufferedReader(
						new InputStreamReader(request.getInputStream(),
								Charset.forName("UTF-8")), 8192)) {
					String s;
					StringBuffer buffer = new StringBuffer();
					while((s = reader.readLine()) != null) {
						buffer.append(s);
						buffer.append(System.getProperty("line.separator"));
					}

					return buffer;

				}
			} else {
				throw new FailedRequestException(request.getResponseCode()
						+ ": " + request.getResponseMessage() + "\n"
						+ request.toString());
			}
		} catch (IOException e) {
			throw new FailedRequestException("expose request failed", e);
		} finally {
			if (request != null) {
				request.disconnect();
			}
		}

	}

	public List<String> getmexposeIds() {
		return mexposeIds;
	}
}
