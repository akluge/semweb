package model;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import model.ontology.OPCIndividualFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import util.OPCObjectType;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static util.Namespace.EXTENSION;
import static util.Namespace.RDF_EXTENSION;

/**
 *  Factory class for creating an ontology model for openStreetMap objects
 */
public class OPCFactory {

    /**
     * create ontology model for openStreetMap resources
     *
     * @param fileName, for file opening
     * @return filled ontology model
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public static OntModel createOPCRDFSchema(String fileName) throws IOException, SAXException, ParserConfigurationException {
        //  String fileName = "bakery";
        final List<OPCObjectType> opcObjectTypes = createOPCObjectsAsList(fileName);

        // create an empty Model
        OntModel m = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
        String opcType = fileName + "/";
        OPCIndividualFactory individualsFactory = new OPCIndividualFactory(m, opcType, fileName);

        for (OPCObjectType opcObjectType : opcObjectTypes) {
            individualsFactory.createOPCIndividual(opcObjectType, m, opcType);
        }

        m.setNsPrefix(fileName, OPCIndividualFactory.NS + opcType);
        m.write(new FileWriter( fileName
                + RDF_EXTENSION), "RDF/XML-ABBREV");

        return m;
    }

    /**
     * parse xml file to openStreetMap object
     *
     * @param fileName, for file opening
     * @return list of openStreetMap objects
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    static List<OPCObjectType> createOPCObjectsAsList(String fileName) throws ParserConfigurationException, IOException, SAXException {
        final List<OPCObjectType> opcObjects = new ArrayList<>();

        File fXmlFile = new File(fileName + EXTENSION);
        if (!fXmlFile.exists())
            throw new FileNotFoundException(fileName+EXTENSION+" not found");

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);

        Node osmRoot = doc.getFirstChild();
        NodeList osmXMLNodes = osmRoot.getChildNodes();

        for (int i = 1; i < osmXMLNodes.getLength(); i++) {
            Node item = osmXMLNodes.item(i);
            if (item.getNodeName().equals("node")) {
                long id;
                Float latitude;
                Float longitude;
                String name = "";
                String typ = "";

                //Id, Long , Lat in Attribute
                NamedNodeMap attributes = item.getAttributes();
                Node namedItemId = attributes.getNamedItem("id");
                Node namedItemLat = attributes.getNamedItem("lat");
                Node namedItemLon = attributes.getNamedItem("lon");
                id = Long.parseLong(namedItemId.getNodeValue());
                latitude = Float.valueOf(namedItemLat.getNodeValue());
                longitude = Float.valueOf(namedItemLon.getNodeValue());

                //child nodes - walk though key value pair
                NodeList tagXMLNodes = item.getChildNodes();
                for (int j = 1; j < tagXMLNodes.getLength(); j++) {
                    Node tagItem = tagXMLNodes.item(j);
                    NamedNodeMap tagAttributes = tagItem.getAttributes();

                    if (tagAttributes != null) {
                        String key = tagAttributes.getNamedItem("k").getNodeValue();
                        String value = tagAttributes.getNamedItem("v").getNodeValue();

                        if (key.equals("name")) {
                            name = value;
                            name = name.replace("[", "");
                            name = name.replace("]", "");
                        }
                        if (key.equals("shop") || key.equals("public_transport")) {
                            typ = value;
                        }
                    }
                }

                OPCObjectType opcObject = new OPCObjectType(name, id, typ, latitude, longitude);

                if (!opcObject.getName().equals("")) {
                    opcObjects.add(opcObject);
                }
            }
        }

        return opcObjects;
    }
}
