package model;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import javafx.util.Pair;
import model.ontology.FlatIndividualFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static util.Namespace.*;

/**
 * Created by Kluge Andreas on 25.12.2015.
 *
 *  Factory class for creating an ontology model for flats
 *
 */
public class FlatFactory {

    /**
     * create ontology model for flat resource
     *
     * @return filled ontology model
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public static OntModel createFlatRDFSchema() throws IOException, SAXException, ParserConfigurationException {
        // create an empty Model
        OntModel m = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
        FlatIndividualFactory individualFactory = new FlatIndividualFactory(m);

        List<Flat> flats;

        try
        {
            FileInputStream fileIn = new FileInputStream(FLAT_SERIALISATION_NAME);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            //noinspection unchecked
            flats = ( List<Flat>) in.readObject();
            in.close();
            fileIn.close();
        }catch(IOException i) {
            System.out.println("File: "+ FLAT_SERIALISATION_NAME +" not found");
            i.printStackTrace();
            flats = new ArrayList<>();
        }catch(ClassNotFoundException c) {
            System.out.println("Flat class not found");
            c.printStackTrace();
            flats = new ArrayList<>();
        }

        //if serialisation file is missing relaunch hole get process if flats.xml exists
        if (flats.isEmpty()) {
            flats = createFlatsAsList();
            Cable ca = new Cable(flats);
            ca.runCableCheck();

            List<OPCObjectType> bakeries = OPCFactory.createOPCObjectsAsList(BAKERY_FILE_NAME);
            List<OPCObjectType> supermarkets = OPCFactory.createOPCObjectsAsList(SUPERMARKET_FILE_NAME);
            List<OPCObjectType> publicTransports = OPCFactory.createOPCObjectsAsList(PUBLIC_TRANSPORT_FILE_NAME);

            DistantCalculator dc = new DistantCalculator(bakeries, supermarkets, publicTransports);
            dc.addFlats(flats);
            flats = dc.run();
        }

        for (Flat flat : flats) {
            individualFactory.createFlatIndividual(flat, m);
        }

        m.setNsPrefix(FLAT_FILE_NAME, FlatIndividualFactory.NS);
        m.write(new java.io.FileWriter(FLAT_FILE_NAME
                + RDF_EXTENSION), "RDF/XML-ABBREV");

        return m;
    }

    /**
     * parse xml file to flat object
     *
     * @return list of flats parsed from xml file
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    private static List<Flat> createFlatsAsList() throws ParserConfigurationException, IOException, SAXException {
        final List<Flat> flats = new ArrayList<>();

        File fXmlFile = new File(FLAT_FILE_NAME + EXTENSION);
        if (!fXmlFile.exists())
            throw new FileNotFoundException(FLAT_FILE_NAME+EXTENSION+" not found");

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);

        Node osmRoot = doc.getFirstChild();
        NodeList osmXMLNodes = osmRoot.getChildNodes();

        for (int i = 1; i < osmXMLNodes.getLength(); i++) {
            Node item = osmXMLNodes.item(i);
            if (item.getNodeName().equals("realEstate")) {
                int id;
                Address address = new Address();
                Flat flat = new Flat(address);
                List<Pair<String, String>> properties = new ArrayList<>();

                //Id in Attribute
                NamedNodeMap attributes = item.getAttributes();
                Node namedItemId = attributes.getNamedItem("id");
                id = Integer.parseInt(namedItemId.getNodeValue());

                //child nodes - below RealEstate
                NodeList tagXMLNodes = item.getChildNodes();
                for (int j = 1; j < tagXMLNodes.getLength(); j++) {
                    Node tagItem = tagXMLNodes.item(j);
                    String name = tagItem.getNodeName();

                    if (name != null) {
                        if (tagItem.getFirstChild() != null) {
                            String value = tagItem.getFirstChild().getNodeValue();
                            if (value != null) {
                                if (!Reducer.checkForFixFlatProperty(name, value, flat))
                                    properties.add(new Pair<>(name, value));
                            }
                        }
                    }
                }

                //Flat flat = new Flat(address, properties);
                flat.setProperties(properties);
                flat.setExposeID(id);
                if (flat.getAddress() != null) {
                    flats.add(flat);
                }

            }
        }

        return flats;
    }

}
