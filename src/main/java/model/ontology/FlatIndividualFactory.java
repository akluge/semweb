package model.ontology;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.vocabulary.XSD;
import util.Flat;

/**
 * Created by Kluge Andreas on 25.12.2015.
 *
 * Create individual objects for flat ontology model
 */
public class FlatIndividualFactory {

    private final OntClass flatClass;

    private final OntProperty idProperty;

    private static OntProperty nameProperty;

    private enum corruptedData {SetHighValue, SetLowValue}
    //necessary information
    // address properties
    private final OntProperty streetProperty;
    private final OntProperty houseNumberProperty;
    private final OntProperty postcodeProperty;
    private final OntProperty quarterProperty;
    private final OntProperty latitudeProperty;
    private final OntProperty longitudeProperty;

    private final OntProperty floorProperty;
    private final OntProperty numberOfRoomsProperty;
    private final OntProperty livingSpaceProperty;
    private final OntProperty rentProperty;
    private final OntProperty builtInKitchenProperty;
    private final OntProperty cableProperty;
    //optional information
    /*
    private final OntProperty heatingCostsInServiceChargeProperty;
    private final OntProperty depositProperty;
    private final OntProperty liftProperty;
    private final OntProperty cellarProperty;
    private final OntProperty handicappedAccessibleProperty;
    private final OntProperty conditionProperty;
    private final OntProperty freeFromProperty;
    private final OntProperty balconyProperty;
    private final OntProperty gardenProperty;
    private final OntProperty heatingTypeProperty;
    private final OntProperty petsAllowedProperty;
    */
    private final OntProperty publicTransportProperty;
    private final OntProperty publicTransportDistanceProperty;
    private final OntProperty bakeryProperty;
    private final OntProperty bakeryDistanceProperty;
    private final OntProperty supermarketProperty;
    private final OntProperty supermarketDistanceProperty;

    public final static String NS = "http://htwk-leipzig.de/andreas-kluge/flat/";

    /**
     * create ontology class for flat ontology model
     *
     * @param model, current model
     */
    public FlatIndividualFactory(final OntModel model) {
        flatClass = model.createClass(NS + "Flat");

        idProperty = model.createOntProperty(NS + "id");
        idProperty.addRange(XSD.xlong);
        idProperty.addDomain(flatClass);

        nameProperty = model.createOntProperty(NS + "name");
        nameProperty.addRange(XSD.xstring);
        nameProperty.addDomain(flatClass);


        publicTransportProperty = model.createOntProperty(NS + "nextPublicTransportID");
        publicTransportProperty.addRange(XSD.xlong);
        publicTransportProperty.addDomain(flatClass);

        publicTransportDistanceProperty = model.createOntProperty(NS + "nextPublicTransportDistance");
        publicTransportDistanceProperty.addRange(XSD.decimal);
        publicTransportDistanceProperty.addDomain(flatClass);

        supermarketProperty = model.createOntProperty(NS + "nextSupermarketID");
        supermarketProperty.addRange(XSD.xlong);
        supermarketProperty.addDomain(flatClass);

        supermarketDistanceProperty = model.createOntProperty(NS + "nextSupermarketDistance");
        supermarketDistanceProperty.addRange(XSD.decimal);
        supermarketDistanceProperty.addDomain(flatClass);

        bakeryProperty = model.createOntProperty(NS + "nextBakeryID");
        bakeryProperty.addRange(XSD.xlong);
        bakeryProperty.addDomain(flatClass);

        bakeryDistanceProperty = model.createOntProperty(NS + "nextBakeryDistance");
        bakeryDistanceProperty.addRange(XSD.decimal);
        bakeryDistanceProperty.addDomain(flatClass);

        streetProperty = model.createOntProperty(NS + "street");
        streetProperty.addRange(XSD.xstring);
        streetProperty.addDomain(flatClass);

        houseNumberProperty = model.createOntProperty(NS + "houseNumber");
        houseNumberProperty.addRange(XSD.xstring);
        houseNumberProperty.addDomain(flatClass);

        postcodeProperty = model.createOntProperty(NS + "postcode");
        postcodeProperty.addRange(XSD.xstring);
        postcodeProperty.addDomain(flatClass);

        quarterProperty = model.createOntProperty(NS + "quarter");
        quarterProperty.addRange(XSD.xstring);
        quarterProperty.addDomain(flatClass);

        latitudeProperty = model.createOntProperty(NS + "latitude");
        latitudeProperty.addRange(XSD.decimal);
        latitudeProperty.addDomain(flatClass);

        longitudeProperty = model.createOntProperty(NS + "longitude");
        longitudeProperty.addRange(XSD.decimal);
        longitudeProperty.addDomain(flatClass);

        floorProperty = model.createOntProperty(NS + "floor");
        floorProperty.addRange(XSD.decimal);
        floorProperty.addDomain(flatClass);

        numberOfRoomsProperty = model.createOntProperty(NS + "numberOfRooms");
        numberOfRoomsProperty.addRange(XSD.decimal);
        numberOfRoomsProperty.addDomain(flatClass);

        livingSpaceProperty = model.createOntProperty(NS + "livingSpace");
        livingSpaceProperty.addRange(XSD.decimal);
        livingSpaceProperty.addDomain(flatClass);

        rentProperty = model.createOntProperty(NS + "totalRent");
        rentProperty.addRange(XSD.decimal);
        rentProperty.addDomain(flatClass);

        builtInKitchenProperty = model.createOntProperty(NS + "builtInKitchen");
        builtInKitchenProperty.addRange(XSD.xstring); //maybe bool
        builtInKitchenProperty.addDomain(flatClass);

        cableProperty = model.createOntProperty(NS + "cable");
        cableProperty.addRange(XSD.xstring);
        cableProperty.addDomain(flatClass);
    }

    /**
     * create individuals form flat object for current model
     *
     * @param flat, contains flat information
     * @param model, current model
     * @return individual for model
     */
    public Individual createFlatIndividual(final Flat flat, final OntModel model) {
        final Individual flatIndividual;
        flatIndividual = model.createIndividual(NS + flat.getId(), flatClass);

        flatIndividual.setPropertyValue(idProperty, ResourceFactory.createTypedLiteral(String.valueOf(flat.getId()), XSDDatatype.XSDlong));

        flatIndividual.setPropertyValue(nameProperty, ResourceFactory.createTypedLiteral(flat.getProperty("title"), XSDDatatype.XSDstring));

        flatIndividual.setPropertyValue(publicTransportProperty, ResourceFactory.createTypedLiteral(flat.getPublicTransport().getId()));
        flatIndividual.setPropertyValue(publicTransportDistanceProperty, ResourceFactory.createTypedLiteral(flat.getPublicTransport().getDist()));

        flatIndividual.setPropertyValue(supermarketProperty, ResourceFactory.createTypedLiteral(flat.getSupermarket().getId()));
        flatIndividual.setPropertyValue(supermarketDistanceProperty, ResourceFactory.createTypedLiteral(flat.getSupermarket().getDist()));

        flatIndividual.setPropertyValue(bakeryProperty, ResourceFactory.createTypedLiteral(flat.getBakery().getId()));
        flatIndividual.setPropertyValue(bakeryDistanceProperty, ResourceFactory.createTypedLiteral(flat.getBakery().getDist()));

        flatIndividual.setPropertyValue(streetProperty, ResourceFactory.createTypedLiteral(flat.getAddress().getStreet()));
        flatIndividual.setPropertyValue(houseNumberProperty, ResourceFactory.createTypedLiteral(flat.getAddress().getHouseNumber()));
        flatIndividual.setPropertyValue(postcodeProperty, ResourceFactory.createTypedLiteral(flat.getAddress().getPostcode()));
        flatIndividual.setPropertyValue(quarterProperty, ResourceFactory.createTypedLiteral(flat.getQuarter()));
        flatIndividual.setPropertyValue(latitudeProperty, ResourceFactory.createTypedLiteral(flat.getLatitude()));
        flatIndividual.setPropertyValue(longitudeProperty, ResourceFactory.createTypedLiteral(flat.getLongitude()));

        int floor = getIntFormPossibleCorruptedData(flat.getFloor());
        int numberOfRooms = getIntFormPossibleCorruptedData(flat.getNumberOfRooms());
        double livingSpace = getDoubleFormPossibleCorruptedData(flat.getLivingSpace(), corruptedData.SetLowValue);
        double baseRent = getDoubleFormPossibleCorruptedData(flat.getBaseRent(), corruptedData.SetHighValue);

        flatIndividual.setPropertyValue(floorProperty, ResourceFactory.createTypedLiteral(floor));
        flatIndividual.setPropertyValue(numberOfRoomsProperty, ResourceFactory.createTypedLiteral(numberOfRooms));
        flatIndividual.setPropertyValue(livingSpaceProperty, ResourceFactory.createTypedLiteral(livingSpace));
        flatIndividual.setPropertyValue(rentProperty, ResourceFactory.createTypedLiteral(baseRent));
        Boolean builtInKitchen = Boolean.parseBoolean(flat.getBuiltInKitchen());
        flatIndividual.setPropertyValue(builtInKitchenProperty, ResourceFactory.createTypedLiteral(builtInKitchen));
        flatIndividual.setPropertyValue(cableProperty, ResourceFactory.createTypedLiteral(flat.hasCable()));

        return flatIndividual;
    }

    /**
     * fix parsing fails
     *
     * @param property current parsed element
     * @param value, set course for fix (to avoid wrong SPARQl query solution)
     * @return parsed element value or fixed value
     */
    private double getDoubleFormPossibleCorruptedData(String property, corruptedData value) {
        double data;
        try {
            data = Double.parseDouble(property);
        } catch (NumberFormatException | NullPointerException e) {
            if (value == corruptedData.SetHighValue) {
                data = Double.MAX_VALUE;
            } else {
                data = 0.0;
            }
        }

        return data;
    }

    /**
     * parse property value with fixing option
     *
     * @param property current parsed element
     * @return parsed value or fixed value
     */
    private int getIntFormPossibleCorruptedData(String property) {
        int data;
        try {
            data = Integer.parseInt(property);
        } catch (NumberFormatException | NullPointerException e) {
            data = 0;
        }

        return data;
    }

}

