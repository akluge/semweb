package model.ontology;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.vocabulary.XSD;
import util.OPCObjectType;

/**
 * Created by Kluge Andreas on 25.12.2015.
 *
 * Create individual objects for OPCObject ontology models
 */
public class OPCIndividualFactory {

    private final OntClass opcClass;

    private final OntProperty idProperty;

    private static OntProperty nameProperty;

    private final OntProperty latitudeProperty;
    private final OntProperty longitudeProperty;

    private final OntProperty typeProperty;

    public final static String NS = "http://htwk-leipzig.de/andreas-kluge/";


    public OPCIndividualFactory(OntModel model, String opcType, String className) {
        opcClass = model.createClass(NS + opcType + className);

        idProperty = model.createOntProperty(NS + opcType + "id");
        idProperty.addRange(XSD.xlong);
        idProperty.addDomain(opcClass);

        nameProperty = model.createOntProperty(NS + opcType + "name");
        nameProperty.addRange(XSD.xstring);
        nameProperty.addDomain(opcClass);

        latitudeProperty = model.createOntProperty(NS + opcType + "latitude");
        latitudeProperty.addRange(XSD.decimal);
        latitudeProperty.addDomain(opcClass);

        longitudeProperty = model.createOntProperty(NS + opcType + "longitude");
        longitudeProperty.addRange(XSD.decimal);
        longitudeProperty.addDomain(opcClass);

        typeProperty = model.createOntProperty(NS + opcType + "type");
        typeProperty.addRange(XSD.xstring);
        typeProperty.addDomain(opcClass);

    }

    public Individual createOPCIndividual(OPCObjectType opcObjectType, OntModel model, String opcType) {

        final Individual opcIndividual = model.createIndividual(NS + opcType + opcObjectType.getId(), opcClass);

        opcIndividual.setPropertyValue(idProperty, ResourceFactory.createTypedLiteral(String.valueOf(opcObjectType.getId()), XSDDatatype.XSDlong));

        opcIndividual.setPropertyValue(nameProperty, ResourceFactory.createTypedLiteral(opcObjectType.getName(), XSDDatatype.XSDstring));

        opcIndividual.setPropertyValue(latitudeProperty, ResourceFactory.createTypedLiteral(opcObjectType.getLat()));
        opcIndividual.setPropertyValue(longitudeProperty, ResourceFactory.createTypedLiteral(opcObjectType.getLon()));

        opcIndividual.setPropertyValue(typeProperty, ResourceFactory.createTypedLiteral(opcObjectType.getTyp(), XSDDatatype.XSDstring));

        return opcIndividual;
    }
}
