package util;


public class FailedRequestException extends Exception {

	private static final long serialVersionUID = -6249853072407815583L;

	public FailedRequestException() {
        super("Request failed");
    }
	
	public FailedRequestException(String message) {
        super(message);
    }
	
	public FailedRequestException(String message, Throwable e) {
        super(message, e);
    }
}
