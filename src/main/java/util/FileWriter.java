package util;

import java.io.*;

/**
 * Created by Kluge Andreas on 07.09.2015.
 *
 * File output
 *
 */
public class FileWriter {

    public static void writeToFile(StringBuffer content,String fileName,String extension) {
        try {
            Writer out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(new File(fileName+ extension)), "UTF8"));
            out.write(content.toString());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
