package util;

import javafx.util.Pair;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static util.Namespace.LINE_SEPARATOR;

/**
 * Created by Andreas Kluge on 07.09.2015.
 *
 * Filter dummyFlat.xml files for appreciable information
 */
public class Reducer implements javax.xml.stream.StreamFilter{

    private static List<String> acceptedTags;
    private static Map<String,String> corruptedNames;

    private static final String STREET = "street";
    private static final String WGS84COORDINATE = "wgs84Coordinate";

    private Reducer() {
        fillAcceptedTags();
        fillCorruptedNames();
    }

    /**
     *  fix names form ImmobilienScout24 xml style
     */
    private void fillCorruptedNames() {
        corruptedNames = new HashMap<>();
        corruptedNames.put("address", "street");
        corruptedNames.put("wgs84Coordinate", "latitude");
    }

    /**
     *  fill appreciable information comparison
     */
    private void fillAcceptedTags() {
        acceptedTags = new ArrayList<>();
        // unique
        acceptedTags.add("realEstate");
        acceptedTags.add("externalId");
        acceptedTags.add("title");
        acceptedTags.add("creationDate");
        // address
        acceptedTags.add("street");
        acceptedTags.add("houseNumber");
        acceptedTags.add("postcode");
        acceptedTags.add("quarter");
        acceptedTags.add("latitude");
        acceptedTags.add("longitude");
        // info
        acceptedTags.add("apartmentType");
        acceptedTags.add("floor");
        acceptedTags.add("lift");
        acceptedTags.add("cellar");
        acceptedTags.add("handicappedAccessible");
        acceptedTags.add("condition");
        acceptedTags.add("freeFrom");
        acceptedTags.add("heatingType");
        acceptedTags.add("firingType");
        acceptedTags.add("numberOfBathRooms");
        acceptedTags.add("guestToilet");
        acceptedTags.add("baseRent");acceptedTags.add("totalRent");
        acceptedTags.add("deposit");
        acceptedTags.add("heatingCostsInServiceCharge");acceptedTags.add("petsAllowed");acceptedTags.add("livingSpace");
        acceptedTags.add("numberOfRooms");
        acceptedTags.add("builtInKitchen");
        acceptedTags.add("balcony");
        acceptedTags.add("garden");
    }

    /**
     * filter method
     *
     * @param filename, to filter
     * @return pair with filtered elements in xml style and as java object representation
     * @throws IOException
     * @throws XMLStreamException
     */
    public static Pair<StringBuffer,Flat> reduceExposeToRelevant(String filename) throws IOException, XMLStreamException {

        XMLInputFactory xmlif = null ;

        try {
            xmlif = XMLInputFactory.newInstance();
            xmlif.setProperty(
                    XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES,
                    Boolean.TRUE);

            xmlif.setProperty(
                    XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES,
                    Boolean.FALSE);

            xmlif.setProperty(
                    XMLInputFactory.IS_NAMESPACE_AWARE,
                    Boolean.TRUE);

            xmlif.setProperty(
                    XMLInputFactory.IS_COALESCING,
                    Boolean.TRUE);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(filename), "UTF8"));


        assert xmlif != null;
        XMLStreamReader xmlr = xmlif.createFilteredReader(
                xmlif.createXMLStreamReader("UTF-8", in),
                new Reducer());

        int eventType = xmlr.getEventType();

        StringBuffer sb = new StringBuffer();
        Address address = new Address("","","");
        List<Pair<String,String>> properties = new ArrayList<>();

        String replaceName = "";

        Flat flat = new Flat(address);

        while (xmlr.hasNext()){
                switch (eventType){
                    case XMLEvent.START_ELEMENT:
                        try {
                            String name = xmlr.getName().toString();
                            if (nameFailure(name)){
                                replaceName = corruptedNames.get(name);
                            } else {
                                String text = xmlr.getElementText();
                                if (acceptedTags.contains(name)) {
                                    text = checkTextForEscapeLiterals(text);
                                    if (!checkForFixFlatProperty(name, text, flat)) {
                                        String nameAndText = "   <" + name + ">" + text + "</" + name + ">";
                                        sb.append(nameAndText);
                                        sb.append(LINE_SEPARATOR);
                                        properties.add(new Pair<>(name, text));
                                    }
                                }
                            }
                        } catch (XMLStreamException e) {
                            // do nothing
                        }
                        break;
                    case XMLEvent.CHARACTERS:
                        if (replaceName.equals(STREET) || replaceName.equals(WGS84COORDINATE)){
                            if (xmlr.getText().contains(LINE_SEPARATOR)) {
                                String text = xmlr.getText();
                                String replaceText = "   <" + replaceName + ">" + text + "</" + replaceName + ">";
                                sb.append(replaceText);
                                properties.add(new Pair<>(replaceName,text));
                            }
                            replaceName = "";
                        }
                        break;
                    default:
                        break;
                }
            if ( (eventType == XMLEvent.START_ELEMENT || eventType == XMLEvent.END_ELEMENT))
                if(xmlr.getName().toString().equals(acceptedTags.get(acceptedTags.size()-1))){
                break;
            }
        eventType = xmlr.next();
        }


        xmlr.close();
        in.close();
        flat.setProperties(properties);
            return new Pair<>(sb,flat);
    }

    /**
     * check for & as troublemaker
     *
     * @param text, to check &
     * @return string without &
     */
    private static String checkTextForEscapeLiterals(String text) {
        return text.replace("&", "und");
    }

    /**
     * check if name should be replaced
     * @param name, to check
     * @return true if name must be replaced
     */
    private static boolean nameFailure(String name) {
        return (corruptedNames.containsKey(name));
    }

    /**
     * check and set a flat property if property is representing in flat ontology model
     *
     * @param name of property
     * @param value of property
     * @param flat, current object to filter
     * @return True if property is representing in flat ontology model
     */
    public static boolean checkForFixFlatProperty(String name, String value, Flat flat) {
        boolean isFixPro = true;
        switch (name) {
            case "street":
                flat.getAddress().setStreet(value);
                break;
            case "houseNumber":
                flat.getAddress().setHouseNumber(value);
                break;
            case "postcode":
                flat.getAddress().setPostCode(value);
                break;
            case "quarter":
                flat.setQuarter(value);
                break;
            case "latitude":
                flat.setLatitude(value);
                break;
            case "longitude":
                flat.setLongitude(value);
                break;
            case "floor":
                flat.setFloor(value);
                break;
            case "livingSpace":
                flat.setLivingSpace(value);
                break;
            case "numberOfRooms":
                flat.setNumberOfRooms(value);
                break;
            case "builtInKitchen":
                flat.setBuiltInKitchen(value);
                break;
            case "baseRent":
                flat.setBaseRent(value);
                break;
            default:
                isFixPro = false;
                break;
        }

        return isFixPro;
    }

    @Override
    public boolean accept(XMLStreamReader reader) {
      //  if (!reader.isStartElement() && !reader.isEndElement())
      //      return false;
      //  else
            return true;
    }
}
