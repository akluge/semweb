package util;

/**
 * Contain nearest distance information
 */
public class Nearest implements java.io.Serializable{

    private final String typ;
    private final double dist;
    private final String name;

    private final long id;

    Nearest(long id, String typ, double dist, String title) {
        this.id = id;
        this.typ = typ;
        this.dist = dist;
        this.name = title;
    }

    public long getId() {
        return id;
    }

    public double getDist() {
        return dist;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Nearest{" +
                "typ='" + typ + '\'' +
                ", dist=" + dist +
                ", name='" + name + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
