package util;

import crawler.DSLConnectionCrawler;
import org.apache.log4j.Category;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Andreas Kluge on 11.12.2015.
 *
 * for cable connection handling
 */
public class Cable {

    private static Logger logger = Logger.getLogger(Cable.class);

    private final List<Flat> flats;

    private final static String SerName = "KnownAddress.ser";

    private int counter;

    private Map<Address, Boolean> knownAddress;

    public Cable(List<Flat> flats) {

        Category root = logger.getParent();
        logger.setLevel(root.getLevel());

        this.flats = flats;

        try {
            FileInputStream fileIn = new FileInputStream(SerName);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            //noinspection unchecked
            knownAddress = (Map<Address, Boolean>) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            System.out.println("File: " + SerName + " not found");
            knownAddress = new HashMap<>();
        } catch (ClassNotFoundException c) {
            System.out.println("Map class not found");
            knownAddress = new HashMap<>();
        }

        counter = 0;

    }

    /**
     * Wrapper class for checking cable connection
     *
     * @return list of flats
     */
    public List<Flat> runCableCheck() {
        DSLConnectionCrawler crawler = new DSLConnectionCrawler();
        logger.debug("already known: " + knownAddress.size());
        for (Flat f : flats){

            counter++;
            if (knownAddress.containsKey(f.getAddress())) {
                boolean hasCable = knownAddress.get(f.getAddress());
                f.setCable(hasCable);
            } else {
                boolean isCable = crawler.isPrimaComAvailable(f.getAddress());
                f.setCable(isCable);
                knownAddress.put(f.getAddress(), isCable);

                serialise();

                try {
                    Thread.sleep(78000);    //wait 1,5min cause anti crawler measures
                } catch (InterruptedException ignored) {

                }
            }
            logger.debug(counter+"/"+flats.size());
        }
        return flats;

    }

    /**
     *  save already queried information
     */
    private void serialise() {
        try {
            FileOutputStream fileOut = new FileOutputStream(SerName);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(knownAddress);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            System.out.println("Serialized data failed");
            i.printStackTrace();
        }

    }

}
