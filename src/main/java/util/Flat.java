package util;

import javafx.util.Pair;

import java.util.List;

/**
 * Contain flat information
 */
public class Flat implements java.io.Serializable{

    private List<Pair<String,String>> properties;
    private Address address;

    private Nearest PublicTransport;
    private Nearest supermarket;
    private Nearest bakery;

    private boolean cable;
    private int exposeID;
    private String quarter;
    private String latitude;
    private String longitude;
    private String floor;
    private String numberOfRooms;
    private String builtInKitchen;
    private String baseRent;
    private String livingSpace;

    public Flat(Address address) {
        this.address = address;
        this.cable = false;
    }

    public void setProperties(List<Pair<String, String>> properties) {
        this.properties = properties;
    }

    public Address getAddress() {
        return address;
    }

    public String getProperty(String property){
        for (Pair<String,String> p : properties){
            if (p.getKey().contains(property))
                return p.getValue();
        }
        return "";
    }

    public Nearest getPublicTransport() {
        return PublicTransport;
    }

    void setPublicTransport(Nearest publicTransport) {
        PublicTransport = publicTransport;
    }

    public Nearest getSupermarket() {
        return supermarket;
    }

    public void setSupermarket(Nearest supermarket) {
        this.supermarket = supermarket;
    }

    public Nearest getBakery() {
        return bakery;
    }

    public void setBakery(Nearest bakery) {
        this.bakery = bakery;
    }

    public boolean hasCable() {
        return cable;
    }

    void setCable(boolean cable) {
        this.cable = cable;
    }

    @Override
    public String toString() {
        return "Flat{" +
                "properties=" + properties +
                ", address=" + address +
                ", PublicTransport=" + PublicTransport +
                ", supermarket=" + supermarket +
                ", bakery=" + bakery +
                ", cable=" + cable +
                ", exposeID=" + exposeID +
                ", quarter='" + quarter + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", floor='" + floor + '\'' +
                ", numberOfRooms='" + numberOfRooms + '\'' +
                ", builtInKitchen='" + builtInKitchen + '\'' +
                ", baseRent='" + baseRent + '\'' +
                '}';
    }

    public void setExposeID(int exposeID) {
        this.exposeID = exposeID;
    }

    public int getId() {
        return exposeID;
    }

    void setQuarter(String quarter) {
        this.quarter = quarter;
    }

    public String getQuarter() {
        return quarter;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLongitude() {
        return longitude;
    }

    void setFloor(String floor) {
        this.floor = floor;
    }

    public String getFloor() {
        return floor;
    }

    void setNumberOfRooms(String numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public String getNumberOfRooms() {
        return numberOfRooms;
    }

    void setBuiltInKitchen(String builtInKitchen) {
        this.builtInKitchen = builtInKitchen;
    }

    public String getBuiltInKitchen() {
        return builtInKitchen;
    }

    void setBaseRent(String baseRent) {
        this.baseRent = baseRent;
    }

    public String getBaseRent() {
        return baseRent;
    }

    void setLivingSpace(String livingSpace) {
        this.livingSpace = livingSpace;
    }

    public String getLivingSpace() {
        return livingSpace;
    }
}
