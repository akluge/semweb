package util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.*;

/**
 *  calculate distance using Haversine-Formel
 */
public class DistantCalculator {

    private final List<OPCObjectType> publicTransports;
    private final List<OPCObjectType> supermarkets;
    private final List<OPCObjectType> bakeries;
    private List<Flat> flats;

    public DistantCalculator(List<OPCObjectType> publicTransports, List<OPCObjectType> supermarkets, List<OPCObjectType> bakeries) {
        this.publicTransports = publicTransports;
        this.supermarkets = supermarkets;
        this.bakeries = bakeries;
    }


    private static double getDistanceFrom(double lat1 , double lng1 , double lat2 , double
            lng2 ) {
        double earthRadius = 6369; // in km
        double dLat = toRadians(lat2 - lat1);
        double dLng = toRadians(lng2 - lng1);
        double a = sin(dLat / 2) * sin(dLat / 2)
                + cos(toRadians(lat1))
                * cos(toRadians(lat2))
                * sin(dLng / 2)
                * sin(dLng / 2);
        double c = 2 * atan2(sqrt(a), sqrt(1 - a));
        double dist = earthRadius * c ;
        return dist * 1000;
    }


    public void addFlats(List<Flat> flats) {
        this.flats = flats;
    }

    public List<Flat> run(){
        double latFlat;
        double lonFlat;

        for (Flat f : flats){
            String lat = f.getLatitude();
            String lon = f.getLongitude();
            if (lat == null || lon == null) { continue; }
            latFlat = Double.parseDouble(lat);
            lonFlat = Double.parseDouble(lon);
            nearestOPC(latFlat, lonFlat, f, publicTransports);
            nearestOPC(latFlat, lonFlat, f, supermarkets);
            nearestOPC(latFlat, lonFlat, f, bakeries);
        }

        return flats;
    }

    private void nearestOPC(double latFlat, double lonFlat, Flat f, List<OPCObjectType> list) {
        double lat;
        double lon;
        double dist;
        int minIndex;
        ArrayList<Double> opcObjectDistances = new ArrayList<>();
        for (OPCObjectType b : list) {
            lat = b.getLat();
            lon = b.getLon();
            dist = getDistanceFrom(latFlat,lonFlat,lat,lon);
            opcObjectDistances.add(dist);
        }
        minIndex = opcObjectDistances.indexOf(Collections.min(opcObjectDistances));
        OPCObjectType opcObjectTyp = list.get(minIndex);
        Nearest nearest = new Nearest(opcObjectTyp.getId(), opcObjectTyp.getTyp(), opcObjectDistances.get(minIndex), opcObjectTyp.getName());
        switch (opcObjectTyp.getTyp()){
            case "stop_position":
                f.setPublicTransport(nearest);
                break;
            case "supermarket":
                f.setSupermarket(nearest);
                break;
            case "bakery":
                f.setBakery(nearest);
                break;
        }
    }
}
