package util;

/**
 * Created by Andreas Kluge on 11.12.2015.
 *
 * for static final Strings
 */
public class Namespace {
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");

    public static final String XML_HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
    public static final String REAL_ESTATE_XMLNS_XSI = "<realEstate xmlns:xsi=";
    public static final String XML_SCHEMA = "\"http://www.w3.org/2001/XMLSchema-instance\" id=\"";

    public static final String EXTENSION = ".xml";
    public static final String RDF_EXTENSION = "RDF.xml";
    public static final String DUMMY_FILE_NAME = "dummy";
    public static final String FLAT_FILE_NAME = "flat";
    public static final String BAKERY_FILE_NAME = "bakery";
    public static final String SUPERMARKET_FILE_NAME = "supermarket";
    public static final String PUBLIC_TRANSPORT_FILE_NAME = "stop_position";
    public static final String FLAT_SERIALISATION_NAME = "flats.ser";

    public static final String REAL_ESTATE_FOOT = "</realEstate>" + LINE_SEPARATOR;




}
