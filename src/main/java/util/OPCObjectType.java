package util;

/**
 * Contain information on bakery, supermarket and public transport stations form OverPass as Object
 */
public class OPCObjectType {

    private final String name;
    private final long id;
    private final String typ;
    private final double lat;
    private final double lon;

    public OPCObjectType(String name, long id, String typ, double lat, double lon) {
        this.name = name;
        this.id = id;
        this.typ = typ;
        this.lat = lat;
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public String getTyp() {
        return typ;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    @Override
    public String toString() {
        return "OPCObjectType{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", typ='" + typ + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                '}';
    }
}
