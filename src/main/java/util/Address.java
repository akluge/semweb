package util;

/**
 * Created by Kluge Andreas on 21.09.2015.
 *
 * Basic class flat address holding
 *
 */
public class Address implements java.io.Serializable{

        private String postcode;
        private String street;
        private String houseNumber;


    Address(String postcode, String street, String houseNumber) {
        this.postcode = postcode;
        this.street = street;
        this.houseNumber = houseNumber;
    }

    public Address() {
    }

    void setPostCode(String postcode) {
        this.postcode = postcode;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getStreet() {
        return street;
    }

    @Override
    public String toString() {
        return "Address{" +
                "postcode='" + postcode + '\'' +
                ", street='" + street + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        return postcode.equals(address.postcode) && street.equals(address.street) && houseNumber.equals(address.houseNumber);

    }

    @Override
    public int hashCode() {
        int result = postcode.hashCode();
        result = 31 * result + street.hashCode();
        result = 31 * result + houseNumber.hashCode();
        return result;
    }

    public String getHouseNumber() {
        return houseNumber;
    }
}
